const {Schema, model} = require('mongoose');

const Note = new Schema({
  userId: {type: String, required: true},
  completed: {type: Boolean, required: true},
  text: {type: String, required: true},
  createdDate: {type: String, required: true}
})

module.exports = model('Note', Note)