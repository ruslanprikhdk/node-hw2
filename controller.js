const User = require('./models/User');
const Role = require('./models/Role');
const Note = require('./models/Note');
const bcrypt = require('bcrypt');
const {validationResult} = require('express-validator');
const jwt = require('jsonwebtoken');
const {secret} = require('./config');

const generateAccessToken = (id, roles) => {
  const payload = {
    id, 
    roles
  }
  return jwt.sign(payload, secret, {expiresIn: '24h'});
}

class Controller {
  async registration(req, res) {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(400).send({message:'Please, check registration credentials'})
      }
      const {username, password} = req.body;
      const candidate = await User.findOne({username});
      if (candidate) {
        return res.status(400).send({message:'This name is already taken'});
      }
      const protectedPassword = await bcrypt.hash(req.body.password, 10);
      const userRole = await Role.findOne({value: 'User'});
      const createdDate = new Date();
      const user = new User ({username, password: protectedPassword, roles: [userRole.value], createdDate: createdDate.toISOString()});
      await user.save();
      return res.status(200).send({message:'User was successfully registered'});
    } catch {
      res.status(500).send({message:'Server error'});
    }     
  }
  async login (req, res) {
    try {
      const {username, password} = req.body;
      const user = await User.findOne({username});
      if(!user) {
        return res.status(400).send({message: `User with name ${username} is not found`});
      }
      const validPassword = bcrypt.compareSync(password, user.password);
      if(!validPassword) {
        return res.status(400).send({message: 'Please, check your password'});
      }
      const token = generateAccessToken(user._id, user.roles);
      return res.status(200).send({message: 'Success', jwt_token: token});
    } catch {
      res.status(500).send({message:'Server error'});
    }   
  }
  async getUserData (req, res) {
    try {
      const userCreds = req.user;
      const user = await User.findById(userCreds.id);
      res.status(200).send({user:{_id: user._id, username: user.username, createdDate: user.createdDate}});
    } catch {
      res.status(400).send({message: 'Please, check whether user exists'});
    }   
  }
  async addNote (req, res) {
    try {
      const userCreds = req.user;
      const user = await User.findById(userCreds.id);
      const {text} = req.body;
      const createdDate = new Date();
      const note = new Note({userId: user._id, completed: false, text: text, createdDate: createdDate.toISOString()});
      await note.save();
      res.status(200).send({message: 'Success'});
    } catch {
      res.status(400).send({message: 'Please, check your input'});
    }   
  }
  async getUserNotes (req, res) {
    try {
      const offset = req.query.offset;
      const limit = req.query.limit;
      console.log(offset);
      console.log(limit);
      const notes = await Note.find();
      const slicedNotes = notes.slice(offset);
      const userCreds = req.user;
      const user = await User.findById(userCreds.id);
      const userNotes = slicedNotes.filter(note => note.userId === userCreds.id);
      const limitedUserNotes = [];
      for (let i = 0; i < userNotes.length; i++) {
        if(i >= limit) break;
        limitedUserNotes.push(userNotes[i]);
      }
      res.status(200).send({offset: Number(offset), limit: Number(limit), count: Number(limitedUserNotes.length), notes: limitedUserNotes});
    } catch {
      res.status(400).send({message: 'Please, check your input'});
    }   
  }
  async getUserNoteById (req, res) {
    try {
      const userCreds = req.user;
      const user = await User.findById(userCreds.id);
      const notes = await Note.find();
      const noteId = req.path.substring(7);
      const filteredNote = notes.filter(note => String(note._id) === noteId)[0];
      if (String(user._id) !== filteredNote.userId) {
        res.status(400).send({message: 'Note is written by another user'});  
      } else {
        res.status(200).send({note:{_id: filteredNote._id, userId: filteredNote.userId, completed: filteredNote.completed, text: filteredNote.text, createdDate: filteredNote.createdDate}});
      }
    } catch {
      res.status(400).send({message: 'Please, check your input'});
    }   
  }
  async updateUserNoteById (req, res) {
    try {
      const userCreds = req.user;
      const {text} = req.body;
      const noteId = req.path.substring(7);
      const note = await Note.findById(noteId);
      const user = await User.findById(userCreds.id);     
      if (String(user._id) !== note.userId) {
        res.status(400).send({message: 'Note is written by another user'});  
      } else {
        note.text = text;
        note.save();
        res.status(200).send({message: 'Success'});
      }                 
    } catch {
      res.status(400).send({message: 'Please, check your input'});
    }   
  }
  async checkUserNoteById (req, res) {
    try {
      const userCreds = req.user;
      const noteId = req.path.substring(7);
      const note = await Note.findById(noteId);
      const user = await User.findById(userCreds.id);     
      if (String(user._id) !== note.userId) {
        res.status(400).send({message: 'Note is written by another user'});  
      } else if (note.completed === false) {
        note.completed = true;
        note.save();
        res.status(200).send({message: 'Success'});
      } else {
        note.completed = false;
        note.save();
        res.status(200).send({message: 'Success'});
      }                 
    } catch {
      res.status(400).send({message: 'Please, check your input'});
    }   
  }
  async deleteUserNoteById (req, res) {
    try {
      const userCreds = req.user;
      const noteId = req.path.substring(7);
      const note = await Note.findById(noteId);
      const user = await User.findById(userCreds.id);     
      if (String(user._id) !== note.userId) {
        res.status(400).send({message: 'Note is written by another user'});  
      } else {
        note.remove();
        res.status(200).send({message: 'Success'});
      }                 
    } catch {
      res.status(400).send({message: 'Please, check your input'});
    }   
  }
  async deleteUser (req, res) {
    try {
      const userCreds = req.user;
      const user = await User.findById(userCreds.id);     
      user.remove();
      res.status(200).send({message: 'Success'});               
    } catch {
      res.status(400).send({message: 'Please, check your input'});
    }   
  }
}

module.exports = new Controller();