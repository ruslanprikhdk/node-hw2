const Router = require('express');
const router = new Router();
const controller = require('./controller');
const {check} = require('express-validator');
const authMiddleware = require('./middleware/authMiddleware');

router.post('/auth/register', [
  check('username', 'No empty username is allowed').notEmpty(),
  check('password', 'Password has to be 4-10 characters long').isLength({min: 4, max:10})
], controller.registration);
router.post('/auth/login', controller.login);
router.post('/addNote', controller.addNote);
router.get('/users/me', authMiddleware, controller.getUserData);
router.delete('/users/me', authMiddleware, controller.deleteUser);
router.post('/notes', authMiddleware, controller.addNote);
router.get('/notes', authMiddleware, controller.getUserNotes);
router.get('/notes/:id', authMiddleware, controller.getUserNoteById);
router.put('/notes/:id', authMiddleware, controller.updateUserNoteById);
router.patch('/notes/:id', authMiddleware, controller.checkUserNoteById);
router.delete('/notes/:id', authMiddleware, controller.deleteUserNoteById);

module.exports = router;