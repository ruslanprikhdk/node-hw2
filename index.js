const express = require('express');
const app = express();
const PORT = process.env.PORT || 8080;
const morgan = require('morgan');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose'); 
const router = require('./router');


app.use(express.json());
app.use(morgan('combined'));
app.use('/api', router);

app.get('/', function(req, res) {
  res.send('Hello, world!');
});


const connect = async () => {
  try{
    await mongoose.connect('mongodb+srv://Ruslan:admin@cluster0.ejdya.mongodb.net/NODE-HW2?retryWrites=true&w=majority')
  } catch (e) {
    console.log(e);
  }
}

connect();

app.listen(PORT, () => {
  console.log('Server is running...');
})